# Chemistry Protocol Table Generator 

Browser based tool to generate Latex code for tables based on given Excel (xlsx) files. 

## Table of Contents
- [Usage](#usage)
    - [Online](#online)
    - [Offline](#offline)
- [Types of Tables](#types-of-tables)
    - [separated header, otherwise no separators](#separated-header-otherwise-no-separators)
    - [Flüssigphase BP1 Table](#flussigphase-bp1-table)
- [Development](#development)

## Usage

### Online

This is the easiest and most convenient way to use this app. Just visit [this website](https://chemistry-protocol-table-generator-latex-tools-087bd37bc3ffe45b.gitlab.io/).

### Offline

You can also use this app offline, so you don't need an internet connection. 
To do this, download this repository once and save it on your PC (you will of course need an internet connection for this).
You can do this by pressing the button `Code` above and choose the option `zip` under `Download source code`. After saving the zip file to your PC you have to extract it which gives you a folder in which the app can be found.
If you want to update the app in the future (if there will be updates) you have to to this again. 

To use the app, simply open the file `browser-app/index.html` in this folder with your browser. It is usually enough to double-click on this file to open it.

### Types of Tables

#### separated header otherwise no separators
Excel: 

![separated header, otherwise no separators](table-types/sh_excel.png)

Latex:

![separated header, otherwise no separators](table-types/sh_latex.png)


#### Flussigphase BP1 Table
Excel (colors not relevant)

![separated header, otherwise no separators](table-types/fbp1_excel.png)

Latex:

![separated header, otherwise no separators](table-types/fbp1_latex.png)


## Development

See the [technical README](quasar-app/README.md)
