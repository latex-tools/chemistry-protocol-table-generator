# Chemistry Protocol Table Generator (table-generator) - Technical README

Browser based tool to generate Latex code for tables based on given Excel (xlsx) files.

## Table of Contents
- [Technologies](#technologies)
- [Run the App](#run-the-app)
  - [Prerequisits](#prerequisites)
  - [Development Mode](#development-mode)
- [Development Tooling](#development-tooling)
  - [Vue Devtools](#vue-devtools)
  - [VS Code](#vs-code)
  - [Docker](#docker)
    - [Get Command Line Access to the Container](#get-command-line-access-to-the-container)
    - [Permission Issues When Creating Files Within the Container](#permission-issues-when-creating-files-within-the-container)
- [Build the App](#build-the-app)
- [Problems](#problems)

## Technologies
This project is based on the following technologies:
- [ Quasar ](https://quasar.dev/)
- [xlsx package](https://www.npmjs.com/package/xlsx)

## Run the App

### Prerequisites

We use [Docker](https://www.docker.com/) to run the server, so you need to install Docker and Docker Compose on the machine where you want to run the server.

### Development Mode

**Start** the server in development mode:
(NOTE: You have to be in the root directory of the git repository)

```bash
docker-compose -f docker-compose.dev.yml up --build
```

_Tip_: You can define a more pleasant git alias for this command:

```bash
git config --local alias.startdev '!docker-compose -f docker-compose.dev.yml up --build'
```

Now you only have to run `git startdev` instead.

**Stop** the server:

```bash
docker-compose -f docker-compose.dev.yml down
```

_Tip_: You can define a more pleasant git alias for this command:

```bash
git config --local alias.stopdev '!docker-compose -f docker-compose.dev.yml down'
```

Now you only have to run `git stopdev` instead.

## Development Tooling

### Vue Devtools

[Vue Devtools](https://devtools.vuejs.org/) are a browser devtools extension for debugging Vue.js applications. See [here](https://devtools.vuejs.org/guide/installation.html) for installation instructions.

### VS Code

VS Code is a popular editor. Of course, you can use the editor you feel most comfortable with. However, if you use VS Code you might find some useful hints here.

Recommended Extensions:

- Vue Language Features (Volar)
- TypeScript Vue Plugin (Volar)
- Quasar Snippets
- ESLint
- Prettier - Code formatter

### Docker

#### Get Command Line Access to the Container

Sometimes it is useful to have command line access to the `table-generator` container. This can be useful to run `quasar` and `npm` commands, e.g. to create new quasar components or install dependencies.

To get access run:

```bash
docker exec -it table-generator /bin/sh
```

_Tip_: You can define a more pleasant git alias for this command:

```bash
git config --local alias.docker-cli '!docker exec -it table-generator /bin/sh'
```

Now you only have to run `git docker-cli` instead.

#### Permission Issues When Creating Files Within the Container

_Note: This problem can only occur when the application is run in [development mode](./run-the-client.md#development-mode)._

If the user you are using docker with on your host system does not have UID 1000, this issue can occur. To check the UID of the current user, run `id -u`.

Quick fix:

Set the environment variable `DOCKER_CLIENT_USER_UID` for docker to the UID of your user you are developing with. There are [several ways](https://docs.docker.com/compose/environment-variables/set-environment-variables/) to do this.

A simple method is to change the Git alias you use to launch the application in [development mode](./run-the-client.md#development-mode) with:

```bash
git config --local alias.startdev '!DOCKER_CLIENT_USER_UID=<UID> docker-compose -f docker-compose.dev.yml up --build'
```

_You have to replace `<UID>` with the UID of your user._

## Build the App

This project uses the `vite-plugin-singlefile` plugin to be able to distribute the application as a single html file (and possibly a few additional resources) so that the app can be used by simply opening the index.html in the browser. In this way, there is no need to install a web server.

Build the app using `quasar build` inside the docker container.
To publish the app for offline use copy the content of the resulting dist directory into the `browser-app` directory in the root directory of the git repository:
```
rm -r browser-app/* && cp -r quasar-app/dist/spa/* browser-app
```

## Problems

### Regarding `vite-plugin-singlefile`

The vite plugin `vite-plugin-singlefile` needs a much higher vite version (5.1.4) than quasar currently provide (2.9.13) (not 100% sure that this is really the problem). Therfore, the dependencies are installed using the `--legacy-peer-deps` option (see Dockerfile) to ignore this problem and the installation error. This seems to work, but remember, that this is a dirty solution and probably break in the future or does not work under all circumstances.

