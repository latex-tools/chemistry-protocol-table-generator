import * as xlsx from 'xlsx';

// H54:O83
export const generateFBP1 = function (
  selectedSheet: xlsx.WorkSheet | null,
  range_field: string
) {
  // console.log(' gen fbp1');
  let generated_latex = '';
  const latex_append = (string: string) => {
    generated_latex = generated_latex.concat(string);
  };
  const invert_dot_colon = (string: string) => {
    const tmp = string.replaceAll('.', '#p');
    const tmp2 = tmp.replaceAll(',', '#k');
    return tmp2.replaceAll('#k', '.').replaceAll('#p', ',');
  };
  const indentation = '      ';

  const range = xlsx.utils.decode_range(range_field);
  const numberOfColumns = range.e.c + 1 - range.s.c;

  const table_start = `
  \\begin{table}[H]
      \\caption{CHANGEME}
      \\centering
      \\begin{tabular}{${'l'.repeat(numberOfColumns)}}
          \\hline
    `;
  const table_end = `
      \\end{tabular}
      \\label{tab:CHANGEME}
  \\end{table}
    `;
  latex_append(table_start);

  for (let R = range.s.r; R <= range.e.r; ++R) {
    latex_append(indentation);
    const secondFieldRefInRow = xlsx.utils.encode_cell({
      c: range.s.c + 1,
      r: R,
    });
    let subsection = false;
    if (
      selectedSheet == null ||
      selectedSheet[secondFieldRefInRow] == null ||
      selectedSheet[secondFieldRefInRow].w == null ||
      selectedSheet[secondFieldRefInRow].w === ''
    )
      subsection = true;

    if (subsection) latex_append(`\\hline\n${indentation}`);

    for (let C = range.s.c; C <= range.e.c; ++C) {
      const cell_address = { c: C, r: R };
      /* if an A1-style address is needed, encode the address */
      const cell_ref = xlsx.utils.encode_cell(cell_address);
      // console.log(cell_ref);
      if (selectedSheet != null) {
        let val = '~';
        if (
          selectedSheet[cell_ref] != null &&
          selectedSheet[cell_ref].w != null
        ) {
          // console.log(cell_ref, selectedSheet.value[cell_ref]);
          val = selectedSheet[cell_ref].w;
          // if (
          //   selectedSheet.value[cell_ref].t != null &&
          //   selectedSheet.value[cell_ref].t === 'n'
          // ) // switch , and . only for fields of type number
          val = invert_dot_colon(val);
        }

        if (C === range.s.c && subsection)
          latex_append(`\\textbf{${val}} & `); // first cell in column
        else if (C === range.e.c)
          latex_append(`${val} `); // last cell in column
        else latex_append(`${val} & `);
      }
    }

    latex_append('\\\\\n');
    if (subsection) latex_append(`${indentation}\\hline\n`);
  }

  latex_append(table_end);
  return generated_latex;
};
