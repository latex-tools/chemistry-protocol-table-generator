import * as xlsx from 'xlsx';
import { generateFBP1 } from './fbp1';
import { generateBasic1 } from './basic1';

export enum TableType {
  BASIC1 = 'basic1',
  FBP1 = 'FBP1',
}

export function getLabelOfTableType(t: TableType): string {
  switch (t) {
    case TableType.BASIC1:
      return 'separated header, otherwise no separators';
      break;
    case TableType.FBP1:
      return 'Flüssigphase BP1 Table';
      break;
  }
}

export function generateLatex(
  selectedSheet: xlsx.WorkSheet | null,
  range_field: string,
  tableType: TableType
): string {
  let generated_latex = '';

  let generatorFunction:
    | ((selectedSheet: xlsx.WorkSheet | null, range_field: string) => string)
    | null = null;
  switch (tableType) {
    case TableType.FBP1:
      generatorFunction = generateFBP1;
      break;

    case TableType.BASIC1:
      generatorFunction = generateBasic1;
      break;
  }

  if (generatorFunction != null)
    generated_latex = generatorFunction(selectedSheet, range_field);

  return generated_latex;
}
