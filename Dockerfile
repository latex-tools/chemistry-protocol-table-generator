# develop stage
FROM node:lts-alpine as develop-stage

# remove default node user
RUN deluser --remove-home node 

ARG USER=client_user
ARG UID=1007
RUN adduser -S -D -s /bin/sh --uid $UID $USER
# To add a user with password
# ARG PASS="password"
# RUN adduser -S -D $USER && echo "$USER:$PASS" | chpasswd

RUN mkdir -p /app
WORKDIR /app

COPY quasar-app/package*.json ./
RUN npm install -g @quasar/cli

RUN rm -rf node_modules && npm install --legacy-peer-deps # installing dependencies and dev depenencies
RUN mv ./node_modules /tmp # moving as backup

# if volumes are used for syncronization between local and container filesystem (f.e. as specified in: docker-compose.dev.yml):
#  if directory 'node_modules' does not exist locally, the directory 'node_modules' would be deleted in the container.
CMD cp -r /tmp/node_modules ./ && npm run dev # therefore restore the backup from '/tmp'



# build stage
FROM develop-stage as build-stage
RUN npm run build

# production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist/spa /usr/share/nginx/html
# RUN npm install --production
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
